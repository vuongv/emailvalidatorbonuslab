package email;
/**
 * 
 * @author khoav
 *
 */
public class Validator {
	//Check if there is one and only one @ 
	public static boolean EmailValidatorAtSignTest(String email) {
		boolean returnCode;
		int counter = 0;
		for (int i = 0; i < email.length(); i++) {
			if (("@").equals(email.substring(i, i+1))) {
				counter ++;
			}
		}
		if (counter == 1) {
			returnCode = true;
		}else {
			returnCode = false;
		}
		return returnCode;
	}
	//Check for Account Name at least 3 alpha-character in lowercase
	public static boolean EmailValidatorAccountNameTest(String email) { 
		boolean returnCode;
		int accountCounter = 0;
		for (int i = 0; i < email.length(); i++) {
			if (email.charAt(i) == '@') {
				break;
			}else {
				if ((email.charAt(i) >= 'a' && email.charAt(i) <= 'z') ) {
					accountCounter ++;
				}
			}
		}
		if(accountCounter >= 3) {
			returnCode = true;
		}else {
			returnCode = false;
		}
		return returnCode;
	}
	//Check for Domain Name at least 3 alpha-character in lowercase 
	// aaa@aaa.com
	public static boolean EmailValidatorDomainNameTest(String email) {
		boolean returnCode;
		int atSign = 0; //1
		int domainCounter = 0; //1
		for (int i = 0; i < email.length(); i++) {
			if (email.charAt(i) == '@') {
				atSign ++;
			}
			if (email.charAt(i) == '.') {
				break;
			}
			if (atSign == 1) {
				if ((email.charAt(i) >= 'a' && email.charAt(i) <= 'z') || (email.charAt(i) >= '0' && email.charAt(i) <= '9') ) {
					domainCounter ++;
				}
			}
		}
		if(domainCounter >= 3 && atSign == 1) {
			returnCode = true;
		}else {
			returnCode = false;
		}
		return returnCode;
	}
	//Check for extension at least 2 alpha-character in extension
	public static boolean EmailValidatorExtensionTest(String email) {
		boolean returnCode = true;
		int dotSign = 0;
		int extensionCount = 0;
		for (int i = 0; i < email.length(); i++) {
			if (email.charAt(i) == '.') {
				dotSign ++;
			}
			if(dotSign == 1) {
				if ((email.charAt(i) >= 'a' && email.charAt(i) <= 'z') ) {
					extensionCount ++;
				}

			}
		}
		if(extensionCount >= 2) {
			returnCode = true;
		}else {
			returnCode = false;
		}
		return returnCode;
	}
}	
