package email;

import static org.junit.Assert.*;

import org.junit.Test;

public class ValidatorTest {

	@Test
	public void EmailValidatorAtSignTestRegular() {
		boolean result = Validator.EmailValidatorAtSignTest("hello@gmail.com");
		assertTrue("Invalid Email", result);
	}
	@Test
	public void EmailValidatorAtSignTestException() {
		boolean result = Validator.EmailValidatorAtSignTest("hello@@gmail.com");
		assertFalse("Too Many or no @ Sign", result);
	}
	@Test
	public void EmailValidatorAtSignTestBoundaryIn() {
		boolean result = Validator.EmailValidatorAtSignTest("hello@gmail.com");
		assertTrue("There can be only one @ sign", result);
	}
	@Test
	public void EmailValidatorAtSignTestBoundaryOut() {
		boolean result = Validator.EmailValidatorAtSignTest("hello@@@@@Gmail.com");
		assertFalse("There are too many @ sign", result);
	}
	@Test
	public void EmailValidatorAccountNameRegular() {
		boolean result = Validator.EmailValidatorAccountNameTest("aaa@gmail.com");
		assertTrue("Invalid Account Name", result);
	}
	@Test
	public void EmailValidatorAccountNameException() {
		boolean result = Validator.EmailValidatorAccountNameTest("aa@gmail.com");
		assertFalse("There is less than 3 lowercase alpha-character in AccountName", result);
	}
	@Test
	public void EmailValidatorAccountNameBoundaryIn() {
		boolean result = Validator.EmailValidatorAccountNameTest("aaaAAA@Gmail.com");
		assertTrue("There are less than 3 lower case alpha-character in AccountName", result);
	}
	@Test
	public void EmailValidatorAccountNameBoundaryOut() {
		boolean result = Validator.EmailValidatorAccountNameTest("AAA@Gmail.com");
		assertFalse("There are no 3 lower case alpha-character in AccountName", result);
	}
	@Test
	public void EmailValidatorDomainNameRegular() {
		boolean result = Validator.EmailValidatorDomainNameTest("aaa@aaa.aa");
		assertTrue("Invalid Domain name", result);
	}
	@Test
	public void EmailValidatorDomainNameException() {
		boolean result = Validator.EmailValidatorDomainNameTest("aaa@aa.aa");
		assertFalse("There is less than 3 lowercase alpha-character in DomainName", result);
	}
	@Test
	public void EmailValidatorDomainNameBoundaryIn() {
		boolean result = Validator.EmailValidatorDomainNameTest("aaa@aaaA.aa");
		assertTrue("Invalid Domain Name", result);
	}
	@Test
	public void EmailValidatorDomainNameBoundaryOut() {
		boolean result = Validator.EmailValidatorDomainNameTest("aaa@AAA.ca");
		assertFalse("Invalid Domain name", result);
	}
	@Test
	public void EmailValidatorExtensionRegular() {
		boolean result = Validator.EmailValidatorExtensionTest("aaa@aa.aa");
		assertTrue("Invalid Extension Name", result);
	}
	@Test
	public void EmailValidatorExtensionException() {
		boolean result = Validator.EmailValidatorExtensionTest("aaa@aa.a");
		assertFalse("Invalid Extension Name", result);
	}
	@Test
	public void EmailValidatorExtensionBoundaryIn() {
		boolean result = Validator.EmailValidatorExtensionTest("aaa@aa.caAA");
		assertTrue("Invalid Extenstion Name", result);
	}
	@Test
	public void EmailValdiatorExtensionBoundaryOut() {
		boolean result = Validator.EmailValidatorExtensionTest("aaa@aa.AA");
		assertFalse("Invalid Extenstion Name", result);
	}
}
